# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Client(models.Model):
    """Procedures for Certification Food"""

    created_on = models.DateTimeField(auto_now_add=True)
    name = models.CharField(max_length=100)
    location = models.TextField(blank=True, null=True)
    food_icon = models.FileField(upload_to='img/', blank=True, null=True)
    logo = models.FileField(upload_to='img/', blank=True, null=True)

    def __str__(self):
        """Return the model as a string."""

        return '{} - {}'.format(self.name, self.location)

class Procedure(models.Model):
    """Procedures for Certification Food"""

    created_on = models.DateTimeField(auto_now_add=True)
    title = models.CharField(max_length=100)
    subtitle = models.TextField(blank=True, null=True)
    is_done = models.BooleanField(default=False)

    def __str__(self):
        """Return the model as a sstring."""

        return '{}'.format(self.title)

class AuditResult(models.Model):
    """Result for Certification Food"""

    client = models.ForeignKey('Client', null=True, blank=True, related_name='client')
    created_on = models.DateTimeField(auto_now_add=True)
    audit_number = models.AutoField(primary_key=True)

    def __str__(self):
        """Return the model as a sstring."""

        return '{}-{}'.format(self.client, self.created_on)

class CheckList(models.Model):
    """Audit Checklist Food"""

    STATUS = (
        ('Not Checked', 'Not Checked'),
        ('OK', 'OK'),
        ('Need Improvement', 'Need Improvement'),
        ('Failed', 'Failed')
    )

    auditresult = models.ForeignKey('AuditResult', null=True, blank=True, related_name='auditresult')
    procedure = models.ForeignKey('Procedure', null=True, blank=True, related_name='procedure')
    created_on = models.DateTimeField(auto_now_add=True)
    comment = models.TextField(blank=True, null=True)
    checkStatus = models.CharField(max_length=20,choices=STATUS,blank=True, null=True)
    score = models.IntegerField(blank=True, null=True)
    image1 = models.FileField(upload_to='img/', blank=True, null=True)
    image2 = models.FileField(upload_to='img/', blank=True, null=True)
    image3 = models.FileField(upload_to='img/', blank=True, null=True)
    def __str__(self):
        """Return the model as a string."""
        return '{}-{}'.format(self.procedure, self.score)
