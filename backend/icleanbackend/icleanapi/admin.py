# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from . import models

# Register your models here.
admin.site.site_header = "iCLEAN Administrator"

admin.site.register(models.Procedure)
admin.site.register(models.CheckList)
admin.site.register(models.Client)
admin.site.register(models.AuditResult)
