from rest_framework import serializers
from . import models


class ClientSerializer(serializers.ModelSerializer):
    """A serializer for Client"""

    class Meta:
        model = models.Client
        fields = '__all__'

class ProcedureSerializer(serializers.ModelSerializer):
    """A serializer for Procedure"""

    class Meta:
        model = models.Procedure
        fields = ('id','title', 'subtitle', 'is_done')

class CheckListSerializer(serializers.ModelSerializer):
    """A serializer for Procedure"""
    detail_procedure = ProcedureSerializer(source='procedure', many=False, read_only=True)

    class Meta:
        model = models.CheckList
        fields = '__all__'


class AuditResultSerializer(serializers.ModelSerializer):
    """A serializer for AuditResult"""
    result_audit = CheckListSerializer(source='auditresult', many=True, read_only=True)
    outlet = ClientSerializer(source='client', many=False, read_only=True)

    class Meta:
        model = models.AuditResult
        fields = '__all__'


class AuditResultDetailSerializer(serializers.ModelSerializer):
    result_audit = CheckListSerializer(source='auditresult', many=True, read_only=True)
    outlet = ClientSerializer(source='client', many=False, read_only=True)
    
    class Meta:
        model = models.AuditResult
        fields = '__all__'
