from django.conf.urls import url
from django.conf.urls import include

from rest_framework.routers import DefaultRouter

from . import views


router = DefaultRouter()
router.register('client', views.ClientViewSet)
router.register('procedure', views.ProcedureViewSet)
router.register('checklist', views.CheckListViewSet)
router.register('auditresult', views.AuditResultViewSet)
# router.register('auditresult/(?P<pk>[0-9]+)/$',views.AuditResultDetail)


urlpatterns = [
    url(r'', include(router.urls)),
    url(r'^auditresult/(?P<pk>[0-9]+)/$',
        views.AuditResultDetail.as_view(), name='auditresult-detail'),
]
