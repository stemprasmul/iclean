# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from rest_framework import viewsets
from rest_framework import generics

from . import serializers
from . import models

# Create your views here.
class ClientViewSet(viewsets.ModelViewSet):
    """Handles CRUD for model Client"""

    serializer_class = serializers.ClientSerializer
    queryset = models.Client.objects.all()

class ProcedureViewSet(viewsets.ModelViewSet):
    """Handles CRUD for model Procedure"""

    serializer_class = serializers.ProcedureSerializer
    queryset = models.Procedure.objects.all()

class CheckListViewSet(viewsets.ModelViewSet):
    """Handles CRUD for model CheckList"""

    serializer_class = serializers.CheckListSerializer
    queryset = models.CheckList.objects.all()

class AuditResultViewSet(viewsets.ModelViewSet):
    """Handles CRUD for model AuditResult"""

    serializer_class = serializers.AuditResultSerializer
    queryset = models.AuditResult.objects.all()

class AuditResultDetail(generics.RetrieveUpdateAPIView):
    """
    get:
    Return detail of region category tour instance.

    put:
    Update an existing region tour category instance.
    """
    queryset = models.AuditResult.objects.all()
    serializer_class = serializers.AuditResultDetailSerializer
