import React from 'react';
import { Navigation } from "react-native-navigation";
import { Provider } from "react-redux";
import configureStore from './src/store/configureStore';
import MainMenu from "./src/screens/MainMenu/MainMenu";
import NewClient from "./src/screens/NewClient/NewClient";
import MakeAudit from "./src/screens/MakeAudit/MakeAudit";
import EditAudit from "./src/screens/MakeAudit/EditAudit";
import UpdateAudit from "./src/screens/MakeAudit/UpdateAudit";
import AuditResult from "./src/screens/AuditResult/AuditResult";
import SelectUpdate from "./src/screens/UpdateAudit/SelectUpdate";
import AuditResultDetail from "./src/screens/AuditResult/AuditResultDetail";

const store = configureStore();


// Register Screens
Navigation.registerComponent(
    "iclean.MainMenu",
    () => MainMenu,
    store,
    Provider
);

Navigation.registerComponent(
    "iclean.MakeAudit",
    () => MakeAudit,
    store,
    Provider
);

Navigation.registerComponent(
    "iclean.EditAudit",
    () => EditAudit,
    store,
    Provider
);

Navigation.registerComponent(
    "iclean.UpdateAudit",
    () => UpdateAudit,
    store,
    Provider
);

Navigation.registerComponent(
    "iclean.AuditResult",
    () => AuditResult,
    store,
    Provider
);

Navigation.registerComponent(
    "iclean.AuditResultDetail",
    () => AuditResultDetail,
    store,
    Provider
);

Navigation.registerComponent(
    "iclean.NewClient",
    () => NewClient,
    store,
    Provider
);

Navigation.registerComponent(
    "iclean.SelectUpdate",
    () => SelectUpdate,
    store,
    Provider
);

Navigation.startSingleScreenApp({
    screen: {
        screen: "iclean.MainMenu",
        title: "ICLEAN"
    },

});

