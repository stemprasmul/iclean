/**
 * Created by mata on 10/15/18.
 */

import React, { Component } from "react";
import {
    View,
    Text,
    Image,
    Dimensions,
    StyleSheet,
    TouchableOpacity,
    TouchableHighlight,
    Button,
    ScrollView,
    Alert
} from "react-native";
import { connect } from "react-redux";
import PickImage from "../../components/PickImage/PickImage";
import FloatingLabel from "react-native-floating-labels";
import { Navigation } from "react-native-navigation";

class NewClient extends Component {
    static navigatorStyle = {
        navBarHidden:true
    };

    state = {
        client_name:'',
        lokasi:'',
        food_icon:null,
        logo:null

    }

    imagePickedHandler1 = image => {
        this.setState({logo:image})
        console.log(this.state.logo)
    };

    imagePickedHandler2 = image => {
        this.setState({food_icon:image})
        console.log(this.state.food_icon)
    };

    submitClientHandler = () => {
        var data = new FormData();
        data.append('name', this.state.client_name)
        data.append('location', this.state.lokasi)
        data.append('logo', {
            uri: this.state.logo.uri, // your file path string
            name: Math.random().toString(36).substr(2, 5)+'.jpg',
            type: 'image/jpg'
        })
        data.append('food_icon', {
            uri: this.state.food_icon.uri, // your file path string
            name: Math.random().toString(36).substr(2, 5)+'.jpg',
            type: 'image/jpg'
        })
        console.log('submitted data', data)

        fetch(
            "http://prasmulpay.com:9998/api/client/",
            {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    //'Content-Type': 'multipart/form-data;',
                    // "Authorization": "Token "+this.state.token
                },
                body:data
            }
        ).then(() =>{
                Alert.alert(
                    'Congrats',
                    '\nNew Client successfully added',
                    [
                        {text: 'OK', onPress: () => {
                            this.imagePicker.reset();
                            //this.locationPicker.reset();
                            Navigation.startSingleScreenApp({
                                screen: {
                                    screen: "iclean.MainMenu",
                                },
                            });
                        }},
                    ],
                    { cancelable: false }
                )
            }
        )
            .catch(err => alert("Adding Client ERROR.\nCheck Again your Network!"));
    }

    render(){
        return(
            <View style={{flex:1}}>
                <View style={{paddingHorizontal:20, height:60, width:'100%', backgroundColor:'white',flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                    <Image
                        style = {{height:35, width:100}}
                        source={require('../../assets/icleanheader.jpg')}
                    />
                </View>
                <ScrollView>
                <View style= {{ flex:1, justifyContent: "center", alignItems: "center"}}>

                    <View style={{paddingVertical:20}}>
                        <Text style={{color:'#56a5d3', fontWeight:'bold', fontSize:20}}>Create a New Client</Text>
                    </View>
                    <View style={{width:"90%",paddingBottom:5}}>
                        <FloatingLabel
                            labelStyle={{fontSize:14, color: '#b4b4b4'}}
                            inputStyle={{borderWidth: 0}}
                            style={styles.formInput}
                            value={this.state.client_name}
                            //onBlur={this.onBlur}
                            onChangeText = {(text)=> this.setState({client_name:text})}
                        >
                            Client Name
                        </FloatingLabel>
                    </View>
                    <View style={{width:"90%"}}>
                        <FloatingLabel
                            labelStyle={{fontSize:14,color: '#b4b4b4'}}
                            inputStyle={{borderWidth: 0}}
                            style={styles.formInput}
                            value={this.state.lokasi}
                            //onBlur={this.onBlur}
                            onChangeText = {(text)=> this.setState({lokasi:text})}
                        >
                            Location
                        </FloatingLabel>
                    </View>
                    <View  style={{paddingTop:10,paddingBottom:5, paddingHorizontal:10, flexDirection: 'row', alignItems:'center', width: "100%",}}>
                        <Text style={{color:"#56a5d3"}}></Text>
                    </View>
                    <PickImage
                        title="Upload Logo Client"
                        onImagePicked={this.imagePickedHandler1}
                        // ref={ref => (this.imagePicker = ref)}
                    />

                    <View  style={{paddingTop:10,paddingBottom:5, paddingHorizontal:10, flexDirection: 'row', alignItems:'center', width: "100%",}}>
                        <Text style={{ color:"#56a5d3"}}></Text>
                    </View>
                    <PickImage
                        title="Upload Food Icon"
                        onImagePicked={this.imagePickedHandler2}
                        // ref={ref => (this.imagePicker = ref)}
                    />


                    <View  style={{paddingTop:40, paddingBottom: 30, alignItems:'center'}}>
                        <TouchableOpacity onPress={this.submitClientHandler}>
                            <View style={{borderRadius: 5, paddingVertical:10,paddingHorizontal:40, backgroundColor:'#56a5d3'}}>
                                <Text style={{paddingHorizontal:10, fontWeight:'bold', fontSize:16,color:'white'}}>Submit New Client</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    formInput: {
        borderColor: '#333',
    }
})

export default connect(null, null)(NewClient);