/**
 * Created by mata on 10/15/18.
 */

/**
 * Created by mata on 10/15/18.
 */

/**
 * Created by mata on 10/15/18.
 */

import React, {Component} from "react";
import {
    View,
    Text,
    Image,
    Dimensions,
    StyleSheet,
    TouchableOpacity,
    TouchableHighlight,
    Button,
    ScrollView,
    ActivityIndicator,
    Alert,
    Picker,
    FlatList
} from "react-native";
import {connect} from "react-redux";
import PickImage from "../../components/PickImage/PickImage";
import FloatingLabel from "react-native-floating-labels";
import {Navigation} from "react-native-navigation";

class AuditResult extends Component {
    static navigatorStyle = {
        navBarHidden: true
    };

    state = {
        audit_results: null,
        isLoading: true
    }

    constructor(props) {
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
    }

    componentWillMount() {
        let url = "https://prasmulpay.com/icleanapi/auditresult/";
        fetch(url, {
            method: 'GET',
        })
            .catch(err => {
                console.log(err);
            })
            .then(res => res.json())
            .then(parsedRes => {
                console.log('audit results: ', parsedRes)
                this.setState({audit_results: parsedRes, isLoading: false})
            });

    }

    detailAuditHandler = (data) => {
        console.log('data detail audit', data)
        this.props.navigator.push({
            screen: "iclean.AuditResultDetail",
            passProps: {
                audit_results: data
            },
        });
    }

    renderContent() {

        if (this.state.isLoading) {
            return (
                <View>
                    <ActivityIndicator size="large" color="#56a5d3"/>
                    <Text style={{color: '#56a5d3'}}>Loading ... </Text>
                </View>
            )
        } else {
            if (this.state.audit_results != null) {
                if (this.state.audit_results.length == 0) {
                    return (
                        <Text>Result is not available yet</Text>
                    )
                } else {
                    return (
                        <FlatList
                            // ListHeaderComponent={this.renderHeader(this.state.products)}
                            contentContainerStyle={styles.listContainer}
                            data={this.state.audit_results}
                            // numColumns={2}
                            keyExtractor={(item, index) => index}
                            renderItem={(info) => {
                                console.log('info.item', info.item)
                                let options = {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'};
                                let stats_ok = 0
                                let stats_needimprove = 0
                                let stats_failed = 0
                                let total_score = 0
                                let length_audit = info.item.result_audit.length

                                for (i = 0; i < info.item.result_audit.length; i++) {
                                    let status = info.item.result_audit[i].checkStatus;

                                    if (status == 'OK') {
                                        stats_ok = stats_ok + 1
                                    }else if(status == 'Need Improvement'){
                                        stats_needimprove = stats_needimprove + 1
                                    }else if(status == 'Failed'){
                                        stats_failed = stats_failed + 1
                                    }
                                    let score = info.item.result_audit[i].score;
                                    total_score = total_score + score
                                }

                                return (
                                    <TouchableOpacity onPress={() => this.detailAuditHandler(info.item)
                                    }>
                                        <View style={styles.listItem}>
                                            <View style={{padding: 5}}>
                                                <View style={{flexDirection: 'row'}}>
                                                    <View style={{paddingLeft: 10}}>
                                                        <Image
                                                            style={{width: 25, height: 25}}
                                                            source={{uri: info.item.outlet.logo}}
                                                        />
                                                    </View>
                                                    <Text style={{
                                                        paddingLeft: 15,
                                                        fontWeight: 'bold',
                                                        fontSize: 16
                                                    }}>{info.item.outlet.name}</Text>
                                                </View>
                                                <View style={{
                                                    margin: 5,
                                                    paddingTop: 10,
                                                    flexDirection: "row",
                                                    flexWrap: "wrap"
                                                }}>
                                                    <Text><Text style={{fontWeight: 'bold'}}>Audit Date
                                                        :</Text> {new Date(info.item.created_on).toLocaleString("en-US", options)}
                                                    </Text>

                                                </View>
                                                <View style={{
                                                    margin: 5,
                                                    flexDirection: "row",
                                                    flexWrap: "wrap"
                                                }}>
                                                    <Text><Text style={{fontWeight: 'bold'}}>Score Average
                                                        :</Text> {(total_score/length_audit).toFixed(2)}
                                                    </Text>
                                                </View>
                                                <View style={{
                                                    margin: 5,
                                                }}>
                                                    <Text>
                                                        <Text style={{fontWeight: 'bold'}}>Summary of Auditing </Text> ({length_audit} procedures) :
                                                    </Text>
                                                    <Text> - OK : {(stats_ok/length_audit*100).toFixed(2)}%  ({stats_ok} items)</Text>
                                                    <Text> - Need Improvement : {(stats_needimprove/length_audit*100).toFixed(2)}%  ({stats_needimprove} items)</Text>
                                                    <Text> - Failed: : {(stats_failed/length_audit*100).toFixed(2)}%  ({stats_failed} items)</Text>
                                                </View>
                                            </View>
                                        </View>
                                    </TouchableOpacity>
                                )
                            }}
                        />
                    )
                }

            }
        }
    }


    render() {

        return (
            <View style={{flex: 1}}>
                <View style={{
                    paddingHorizontal: 5,
                    height: 60,
                    width: '100%',
                    backgroundColor: 'white',
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <Image
                        style={{height: 35, width: 100}}
                        source={require('../../assets/icleanheader.jpg')}
                    />
                </View>
                <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
                    <View style={{width: '99%'}}>
                        <View style={{justifyContent: 'center', alignItems: 'center'}}>
                            {this.renderContent()}
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    listContainer: {
        width: "100%"
    },
    listItem: {
        width: (Dimensions.get('window').width - 40),
        margin: 5,
        paddingVertical: 10,
        backgroundColor: "white",
        flexDirection: "column",
        borderRadius: 5,
        borderWidth: 0.5,
        borderColor: '#ADAEAD',
        shadowColor: '#ADAEAD',
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 3,
    },
})

export default connect(null, null)(AuditResult);