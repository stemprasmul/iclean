/**
 * Created by mata on 10/21/18.
 */

import React, {Component} from "react";
import {
    View,
    Text,
    Image,
    Dimensions,
    StyleSheet,
    TouchableOpacity,
    TouchableHighlight,
    Button,
    ScrollView,
    ActivityIndicator,
    Alert,
    Picker,
    FlatList
} from "react-native";
import {connect} from "react-redux";
import Carousel from 'react-native-snap-carousel';

class AuditResultDetail extends Component {
    static navigatorStyle = {
        navBarHidden: true
    };

    constructor(props) {
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
        console.log('data audit', this.props.audit_results)
    }

    _renderItem ({item, index}) {
        return (
            <View style = {{height:100,justifyContent:'center', alignItems:'center'}}>
                <View style={{paddingTop:5}}>
                    <Image
                        style={{width: 130, height: 100}}
                        source={{uri: item.img}}
                    />
                </View>
            </View>
        );
    }

    render() {
        let options = {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'};
        return (
            <View style={{flex: 1}}>
                <View style={{
                    paddingHorizontal: 5,
                    height: 60,
                    width: '100%',
                    backgroundColor: 'white',
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center'
                }}>
                    <Image
                        style={{height: 35, width: 100}}
                        source={require('../../assets/icleanheader.jpg')}
                    />
                </View>
                <View style={{flex: 1}}>
                    <View style={{padding:10}}>
                        <Text style={{fontWeight:'bold', fontSize:14, color:'#56a5d3'}}>{this.props.audit_results.outlet.name}</Text>
                        <Text>Location : {this.props.audit_results.outlet.location}</Text>
                        <Text>Audit date : {new Date(this.props.audit_results.created_on).toLocaleString("en-US", options)}</Text>
                    </View>
                    <View style={{flex:1,justifyContent: "center", alignItems: "center"}}>
                        <View>
                            <FlatList
                                // ListHeaderComponent={this.renderHeader(this.state.products)}
                                contentContainerStyle={styles.listContainer}
                                data={this.props.audit_results.result_audit}
                                // numColumns={2}
                                keyExtractor={(item, index) => index}
                                renderItem={(info) => {
                                    return(
                                        <View style={styles.listItem}>
                                            <Text style={{fontWeight:'bold', color:'#56a5d3'}}>{info.item.detail_procedure.title}</Text>
                                            <View style={{padding:5, borderBottomWidth:0.5}}>
                                                <Text style={{fontSize:10}}>{info.item.detail_procedure.subtitle}</Text>
                                            </View>
                                            <Text><Text style={{fontWeight:'bold'}}>Score : </Text>{info.item.score}</Text>
                                            <Text><Text style={{fontWeight:'bold'}}>Status: </Text>{info.item.checkStatus}</Text>
                                            <Text><Text style={{fontWeight:'bold'}}>Comments:</Text></Text>
                                            <Text style={{paddingLeft:10}}>{info.item.comment}</Text>
                                            <Text style={{fontWeight:'bold'}}>Images:</Text>
                                            <View style={{width:'100%', justifyContent:'center', alignItems:'center'}}>
                                                <Carousel
                                                    data={[{img:info.item.image1},{img:info.item.image2},{img:info.item.image3}]}
                                                    renderItem={this._renderItem}
                                                    sliderWidth={400}
                                                    itemWidth={130}
                                                    loop={true}
                                                />
                                            </View>
                                        </View>
                                    )
                                }}
                            />
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    listContainer: {
        width:'100%',
    },
    listItem: {
        width: (Dimensions.get('window').width - 10),
        margin: 5,
        padding: 10,
        backgroundColor: "white",
        flexDirection: "column",
        borderRadius: 5,
        borderWidth: 0.5,
        borderColor: '#56a5d3',
        shadowColor: '#56a5d3',
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 3,
    },
})

export default connect(null, null)(AuditResultDetail);