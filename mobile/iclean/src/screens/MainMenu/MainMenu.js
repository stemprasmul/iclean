/**
 * Created by mata on 10/14/18.
 */


import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Text,
    Image,
    TouchableOpacity,
    ActivityIndicator
} from "react-native";
import { connect } from "react-redux";
import Carousel from 'react-native-snap-carousel';
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

class MainMenu extends Component {
    static navigatorStyle = {
        navBarHidden:true
    };

    state = {
        errorMessage:null,
        isLoading:false,
        clients: null
    }

    constructor(props) {
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
    }

    componentDidMount(){
        let url = "https://prasmulpay.com/icleanapi/client/";
                fetch(url, {
                    method: 'GET',
                })
                    .catch(err => {
                        console.log(err);
                        this.setState({ errorMessage:err, isLoading:false})
                    })
                    .then(res => res.json())
                    .then(parsedRes => {
                        console.log(parsedRes)
                        this.setState({clients: parsedRes, isLoading:false})
                    });

    }

    newClientHandler = () => {
        this.props.navigator.push({
                screen: "iclean.NewClient",
        });
    }

    auditResultHandler = () => {
        this.props.navigator.push({
            screen: "iclean.AuditResult",
        });
    }

    makeAuditHandler = () => {
        this.props.navigator.push({
            screen: "iclean.MakeAudit",
            passProps: {
                clients: this.state.clients
            },
        });
    }

    updateDataHandler = () => {
        this.props.navigator.push({
            screen: "iclean.SelectUpdate",
            // passProps: {
            //     auditnumber:  31
            // },
        });
    }


    _renderItem ({item, index}) {
        return (
            <View style = {{height:200, backgroundColor:'white',  borderWidth:1, borderColor:'#56a5d3',  justifyContent:'center', alignItems:'center'}}>
                <View style={{paddingTop:5}}>
                    <Image
                        style={{width: 200, height: 150}}
                        source={{uri: item.food_icon}}
                    />
                </View>
                <View style={{flexDirection:'row', paddingTop:10}}>
                    <Image
                        style={{width: 25, height: 25}}
                        source={{uri: item.logo}}
                    />
                    <View style={{justifyContent:'center', alignItems:'center'}}>
                        <Text style={{paddingLeft: 10, fontSize:14, fontWeight:'bold'}}>{ item.name }</Text>
                    </View>
                </View>

            </View>
        );
    }

    render(){
        let carousel = null
        if(this.state.clients != null){
            carousel = (
                <Carousel
                    ref={(c) => { this._carousel = c; }}
                    data={this.state.clients}
                    renderItem={this._renderItem}
                    sliderWidth={300}
                    itemWidth={250}
                    loop={true}
                />
            )
        }else {
            carousel = (
                <View style={{height:200, justifyContent:'center', alignItems:'center'}}>
                    <ActivityIndicator size="large" color="white" />
                </View>
            )
        }
        return(
            <View style={{flex:1}}>
                <View style={{paddingHorizontal:20, height:60, width:'100%', backgroundColor:'white',flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                    <Image
                        style = {{height:35, width:100}}
                        source={require('../../assets/icleanheader.jpg')}
                    />
                </View>
                <View style = {{backgroundColor:'#56a5d3', paddingBottom:20}}>
                    <View style={{padding:10}}>
                        <Text style={{color:'white', fontWeight:'bold'}}>Our Clients: </Text>
                    </View>
                    <View style={{width:'100%', justifyContent:'center', alignItems:'center'}}>
                        {carousel}
                    </View>
                </View>
                <View style={{paddingTop: 20, flexDirection:'row', width:'100%', justifyContent:'space-around', alignItems:'center'}}>
                    <View style={{padding:20}}>
                        <TouchableOpacity onPress={this.makeAuditHandler}>
                            <View style={{ justifyContent:'center', alignItems:'center'}}>
                                <Icon
                                    name={"md-search"}
                                    size={40}
                                    color="#56a5d3"
                                />

                                <Text style={{fontSize:14}}>Make Audit</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{padding:20}}>
                        <TouchableOpacity onPress={this.auditResultHandler}>
                            <View style={{ justifyContent:'center', alignItems:'center'}}>
                                <Icon
                                    name={"md-list-box"}
                                    size={40}
                                    color="#56a5d3"
                                />

                                <Text style={{fontSize:14}}>Audit Result</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{flexDirection:'row', width:'100%', justifyContent:'space-around', alignItems:'center'}}>
                    <View style={{padding:20}}>
                        <TouchableOpacity onPress={this.newClientHandler}>
                            <View style={{ justifyContent:'center', alignItems:'center'}}>
                                <Icon
                                    name={"md-person-add"}
                                    size={40}
                                    color="#56a5d3"
                                />

                                <Text style={{fontSize:14}}>Create new Client</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                    <View style={{padding:20}}>
                        <TouchableOpacity onPress={this.updateDataHandler}>
                            <View style={{ justifyContent:'center', alignItems:'center'}}>
                                <MaterialCommunityIcons
                                    name={"update"}
                                    size={40}
                                    color="#56a5d3"
                                />

                                <Text style={{fontSize:14}}>Update Audit</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({

})

export default connect(null, null)(MainMenu);