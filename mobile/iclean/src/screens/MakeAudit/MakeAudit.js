/**
 * Created by mata on 10/15/18.
 */

/**
 * Created by mata on 10/15/18.
 */

import React, { Component } from "react";
import {
    View,
    Text,
    Image,
    Dimensions,
    StyleSheet,
    TouchableOpacity,
    TouchableHighlight,
    Button,
    ScrollView,
    ActivityIndicator,
    Alert,
    Picker,
    FlatList
} from "react-native";
import { connect } from "react-redux";
import PickImage from "../../components/PickImage/PickImage";
import FloatingLabel from "react-native-floating-labels";
import { Navigation } from "react-native-navigation";

class MakeAudit extends Component {
    static navigatorStyle = {
        navBarHidden:true
    };

    state = {
        client:'',
        auditnumber:'',
        isAuditStarted:false,
        errorMessage:null,
        procedures:null
    }

    constructor(props) {
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
        console.log('clients', this.props.clients)
    }

    componentWillMount(){
        let url = "https://prasmulpay.com/icleanapi/procedure/";
        fetch(url, {
            method: 'GET',
        })
            .catch(err => {
                console.log(err);
                this.setState({ errorMessage:err})
            })
            .then(res => res.json())
            .then(parsedRes => {
                console.log(parsedRes)
                this.setState({procedures: parsedRes})
            });

    }

    editProcedureHandler = (proc) => {
        // console.log('handler proc', proc)

        this.props.navigator.push({
            screen: "iclean.EditAudit",
            passProps: {
                procedure: proc,
                auditnumber:this.state.auditnumber
            },
        });
    }

    renderContent() {
        console.log(console.log('procedures', this.state.procedures, this.state.isAuditStarted))

        if(this.state.isAuditStarted){
            return(
                <FlatList
                    // ListHeaderComponent={this.renderHeader(this.state.products)}
                    contentContainerStyle={styles.listContainer}
                    data={this.state.procedures}
                    // numColumns={2}
                    keyExtractor={(item, index) => index}
                    renderItem={(info) =>
                    {
                        // console.log('info', info)
                        return(
                            <TouchableOpacity onPress={() => this.editProcedureHandler(info.item)}>
                                <View style={styles.listItem}>
                                    <View style={{padding:5}}>
                                        <Text style={{fontWeight:'bold', fontSize:16}}>{info.item.title}</Text>
                                        <View style={{borderRadius: 5,borderWidth:0.25, margin:5, padding:10, flexDirection: "row", flexWrap: "wrap" }}>
                                            <Text>{info.item.subtitle}</Text>
                                        </View>
                                        <View style={{flexDirection:'row', justifyContent:'flex-end', paddingRight:20}}>
                                            <View>
                                                <Text style={{fontWeight:'bold'}}>{!info.item.is_done?'Not Checked':'Query ke Model Checklist'}</Text>
                                            </View>
                                        </View>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        )
                    }}
                />
            )
        }
    }

    startAuditHandler = () => {
        if(this.state.client == ''){
            Alert.alert('ERROR', 'Silahkan pilih client terlebih dahulu')
        }else{
            let url = 'https://prasmulpay.com/icleanapi/auditresult/';
            var data = new FormData();
            data.append('client', this.state.client)

            console.log('submitted data', data)

            fetch(
                url,
                {
                    method: "POST",
                    headers: {
                        'Accept': 'application/json',
                        //'Content-Type': 'multipart/form-data;',
                        // "Authorization": "Token "+this.state.token
                    },
                    body:data
                }
            ).then(res => res.json())
             .then(res =>{
                    console.log('Audit result successfully added', res)
                    this.setState({isAuditStarted:!this.state.isAuditStarted, auditnumber:res.audit_number})
                }
            )
                .catch(err => alert("Adding Client ERROR.\nCheck Again your Network!"));
        }

    }

    render(){
        let pickerItemScope = [];
        if (this.props.clients != null) {
            pickerItemScope = this.props.clients.map((item, key) => {
                console.log('item', item, key)
                return (
                    //   <Picker.Item key={'d' + dept.id} label={dept.name} value={dept.id} />
                    <Picker.Item label={item.name} value={item.id} key={key}/>
                )
            })
        }

        return(
            <View style={{flex:1}}>
                <View style={{paddingHorizontal:20, height:60, width:'100%', backgroundColor:'white',flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                    <Image
                        style = {{height:35, width:100}}
                        source={require('../../assets/icleanheader.jpg')}
                    />
                </View>
                <ScrollView>
                    <View style= {{ flex:1, justifyContent: "center", alignItems: "center"}}>
                        <View style={{paddingVertical:20, width:'90%'}}>
                            <Text style={{color:'#56a5d3', fontWeight:'bold', fontSize:20}}>Select Client</Text>
                            <Picker
                                selectedValue={this.state.client}
                                style={{ color: '#c5c5c5' }}
                                onValueChange={(itemValue, itemIndex) => {
                                    console.log('item value index', itemValue, itemIndex)
                                    if(itemValue == 'null'){
                                        Alert.alert("ERROR","Silahkan pilih client untuk di audit")
                                    }else{
                                        this.setState({ client: itemValue})
                                    }
                                }
                                }>
                                <Picker.Item label="Select client" value="null" />
                                {pickerItemScope}
                            </Picker>
                            <View style={{paddingTop:10, justifyContent:'center', alignItems:'center'}}>
                                <TouchableOpacity onPress={this.startAuditHandler}>
                                    <View style={{ height: 40, width:200, backgroundColor: '#56a5d3',justifyContent:'center', alignItems:'center'}}>
                                        <Text style={{fontSize:14, color:'white'}}>{!this.state.isAuditStarted?'START AUDIT':'CANCEL AUDIT'}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            <View style={{flex:1,justifyContent:'center', alignItems:'center', paddingTop:10}}>
                                {this.renderContent()}
                                {this.state.errorMessage}
                            </View>
                        </View>

                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    listContainer : {
        width:"100%"
    },
    listItem: {
        width: (Dimensions.get('window').width-50),
        margin: 5,
        paddingVertical:10,
        backgroundColor: "#e5e5e5",
        flexDirection: "column",
        borderRadius: 5,
        borderWidth:0.5,
        borderColor:'#ADAEAD',
        shadowColor: '#ADAEAD',
        shadowOpacity: 0.8,
        shadowRadius: 5,
        elevation: 3,
    },
})

export default connect(null, null)(MakeAudit);


// Create Object AuditResult
// {
//     "client": null
// }

// TODO get procedure
//     Create objek Check List
//     "id": 7,
//     "procedure": null,
//     "created_on": "2018-10-21T15:17:19.246057Z",
//     "comment": "ttsetset",
//     "checkStatus": "Failed",
//     "score": 9,
//     "image1": null,
//     "image2": null,
//     "image3": null,
//     "auditresult": 3