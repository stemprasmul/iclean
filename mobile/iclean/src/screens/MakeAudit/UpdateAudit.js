/**
 * Created by mata on 10/24/18.
 */

/**
 * Created by mata on 10/19/18.
 */

/**
 * Created by mata on 10/15/18.
 */

/**
 * Created by mata on 10/15/18.
 */

import React, { Component } from "react";
import {
    View,
    Text,
    Image,
    Dimensions,
    StyleSheet,
    TouchableOpacity,
    TouchableHighlight,
    Button,
    ScrollView,
    ActivityIndicator,
    Alert,
    Picker,
    FlatList
} from "react-native";
import { connect } from "react-redux";
import Icon from 'react-native-vector-icons/Ionicons';
import { Navigation } from "react-native-navigation";

class UpdateAudit extends Component {
    static navigatorStyle = {
        navBarHidden:true
    };

    state = {
        combineaudit:null,
        auditresult:null
    }

    constructor(props) {
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
        console.log('auditnumber', this.props.auditnumber)
    }

    componentWillMount(){
        let url = "https://prasmulpay.com/icleanapi/auditresult/"+this.props.auditnumber+'/';

        fetch(url, {
            method: 'GET',
        })
            .catch(err => {
                console.log(err);
                // this.setState({ errorMessage:err})
            })
            .then(res => res.json())
            .then(parsedRes => {
                console.log('auditresult',parsedRes)
                this.setState({auditresult: parsedRes})


                let url2 = "https://prasmulpay.com/icleanapi/procedure/";
                fetch(url2, {
                    method: 'GET',
                })
                    .catch(err => {
                        console.log(err);
                        this.setState({ errorMessage:err})
                    })
                    .then(res => res.json())
                    .then(parsedRes => {
                        console.log(parsedRes)

                        let validAudit = []
                        let temp = {}
                        let currentAudit = this.state.auditresult.result_audit;

                        console.log('currentAudit', currentAudit)
                        console.log('procedures', parsedRes)

                        //TODO match procedure and audit result
                         for(i=0;i<parsedRes.length;i++){
                             temp = {}
                             temp.title = parsedRes[i].title
                             temp.proc_id = parsedRes[i].id
                             if(currentAudit.length == 0){
                                 temp.status = 'Not Available'
                                 temp.score = 'Not Available'
                                 temp.procedure = parsedRes[i]
                                 temp.auditnumber = this.state.auditresult.audit_number
                             }
                             for(j=0;j<currentAudit.length;j++){
                                 temp.auditnumber = currentAudit[j].auditresult
                                if(parsedRes[i].title == currentAudit[j].detail_procedure.title){
                                     console.log('heroooo', parsedRes[i], currentAudit[j])

                                    temp.status = currentAudit[j].checkStatus
                                    temp.score  = currentAudit[j].score
                                    temp.procedure = currentAudit[j].detail_procedure
                                    break;
                                }else{
                                    temp.status = 'Not Available'
                                    temp.score = 'Not Available'
                                    temp.procedure = parsedRes[i]
                                }
                             }
                             validAudit.push(temp)
                         }
                        this.setState({combineaudit: validAudit})
                    });
            });



    }

    renderStatus(status){
        if(status == 'OK'){
            return(
                <Text style={{color:'green'}}>OK</Text>
            )
        }else if(status == 'Not Available'){
            return(
                <Text style={{color:'red'}}>Not Available</Text>
            )
        }else{
            return(
                <Text style={{}}>{status}</Text>
            )
        }
    }

    editProcedureHandler = (info) => {
        console.log('handler proc', info)

        this.props.navigator.push({
            screen: "iclean.EditAudit",
            passProps: {
                procedure: info.procedure,
                auditnumber:info.auditnumber
            },
        });
    }

    renderContent() {
        if(this.state.auditresult){
            let options = {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'};

            return(
                <View>
                    <View style={{padding:10, width:'100%'}}>
                        <View style={{paddingBottom:10}}>
                            <Text style={{fontSize:16}}><Text style={{fontWeight:'bold'}}>Outlet: </Text>{this.state.auditresult.outlet.name}</Text>
                            <Text style={{fontSize:16}}><Text style={{fontWeight:'bold'}}>Lokasi: </Text>{this.state.auditresult.outlet.location}</Text>
                            <Text>{new Date(this.state.auditresult.outlet.created_on).toLocaleString("en-US", options)}</Text>
                        </View>
                    </View>
                    <FlatList
                        contentContainerStyle={styles.listContainer}
                        data={this.state.combineaudit}
                        keyExtractor={(item, index) => index}
                        renderItem={(info) =>
                        {
                            // console.log('info', info)
                            return(
                                <TouchableOpacity disabled={info.item.status != 'Not Available'} onPress={() => this.editProcedureHandler(info.item)}>
                                    <View style={styles.listItem}>
                                        <View style={{padding:10}}>

                                            <View style={{borderRadius: 5,borderWidth:0.25, margin:5, padding:10, flexWrap: "wrap" }}>
                                                <Text style={{fontWeight:'bold', fontSize:16}}>{info.item.title}</Text>
                                                <View style={{justifyContent:'flex-end', paddingRight:20}}>
                                                    <Text style={{}}>Score : {info.item.score}</Text>
                                                    <Text style={{}}>Audit Result : {this.renderStatus(info.item.status)}</Text>
                                                </View>
                                            </View>

                                        </View>
                                    </View>
                                </TouchableOpacity>
                            )
                        }}
                    />
                </View>
            )
        }
    }

    gotoHome = () => {
        Navigation.startSingleScreenApp({
            screen: {
                screen: "iclean.MainMenu",
            },
        })
    }

    render(){
        return(
            <View style={{flex:1}}>
                <View style={{width:'100%',flexDirection:'row', justifyContent:'flex-start', alignItems:'center',  backgroundColor:'white'}}>
                    <View style={{paddingLeft:10}}>
                        <TouchableOpacity onPress={this.gotoHome}>
                            <View style={{ justifyContent:'center', alignItems:'center'}}>
                                <Icon
                                    name={"md-home"}
                                    size={35}
                                    color="#56a5d3"
                                />
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={{paddingLeft:80, paddingHorizontal:20,height:60,flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                        <Image
                            style = {{height:35, width:100}}
                            source={require('../../assets/icleanheader.jpg')}
                        />
                    </View>
                </View>
                <View style= {{paddingTop:20, flex:1, justifyContent: "center", alignItems: "center"}}>
                    <View style={{}}>
                        <Text style={{color:'#56a5d3', fontWeight:'bold', fontSize:20}}>Updating Data</Text>
                    </View>
                    {this.renderContent()}
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({

})

export default connect(null, null)(UpdateAudit);
