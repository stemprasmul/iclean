/**
 * Created by mata on 10/19/18.
 */

/**
 * Created by mata on 10/15/18.
 */

/**
 * Created by mata on 10/15/18.
 */

import React, { Component } from "react";
import {
    View,
    Text,
    Image,
    Dimensions,
    StyleSheet,
    TouchableOpacity,
    TouchableHighlight,
    Button,
    ScrollView,
    ActivityIndicator,
    Alert,
    Picker,
    FlatList
} from "react-native";
import { connect } from "react-redux";
import PickImage from "../../components/PickImage/PickImage";
import { Navigation } from "react-native-navigation";
import DefaultInput from "../../components/DefaultInput/DefaultInput";

class EditAudit extends Component {
    static navigatorStyle = {
        navBarHidden:true
    };

    state = {
        comment:'',
        checkstatus:'',
        score:'',
        image1:null,
        image2:null,
        image3:null
    }

    constructor(props) {
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent);
        console.log('proc:', this.props.procedure)
        console.log('auditnumber', this.props.auditnumber)
    }

    componentWillMount(){
        // let url = "https://prasmulpay.com/icleanapi/procedure/";
        // fetch(url, {
        //     method: 'GET',
        // })
        //     .catch(err => {
        //         console.log(err);
        //         this.setState({ errorMessage:err})
        //     })
        //     .then(res => res.json())
        //     .then(parsedRes => {
        //         console.log(parsedRes)
        //         this.setState({procedures: parsedRes})
        //     });

    }

    imagePickedHandler1 = image => {
        this.setState({image1:image})
    };
    imagePickedHandler2 = image => {
        this.setState({image2:image})
    };
    imagePickedHandler3 = image => {
        this.setState({image3:image})
    };

    submitAuditHandler = () => {
        let url = 'https://prasmulpay.com/icleanapi/checklist/';

        var data = new FormData();
        data.append('comment', this.state.comment)
        data.append('checkStatus', this.state.checkstatus)
        data.append('score', this.state.score)

        data.append('auditresult', this.props.auditnumber)
        data.append('procedure', this.props.procedure.id)

        if(this.state.image1){
            data.append('image1', {
                uri: this.state.image1.uri, // your file path string
                name: Math.random().toString(36).substr(2, 5)+'.jpg',
                type: 'image/jpg'
            })
        }

        if(this.state.image2){
            data.append('image2', {
                uri: this.state.image2.uri, // your file path string
                name: Math.random().toString(36).substr(2, 5)+'.jpg',
                type: 'image/jpg'
            })
        }

        if(this.state.image3){
            data.append('image3', {
                uri: this.state.image3.uri, // your file path string
                name: Math.random().toString(36).substr(2, 5)+'.jpg',
                type: 'image/jpg'
            })
        }


        console.log('submitted Check List data', data)

        fetch(
            url,
            {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    //'Content-Type': 'multipart/form-data;',
                    // "Authorization": "Token "+this.state.token
                },
                body:data
            }
        ).then(res => {
            console.log('res',res)
            res.json()
        })
            .then(res =>{
                    console.log('Check List successfully added', res)
                    // this.props.navigator.push({
                    //     screen: "iclean.UpdateAudit",
                    //     passProps: {
                    //         auditnumber:  this.props.auditnumber
                    //     },
                    // });
                    Navigation.startSingleScreenApp({
                        screen: {
                            screen: "iclean.UpdateAudit",
                            title: "ICLEAN"
                        },
                        passProps:{
                            auditnumber: this.props.auditnumber
                        }

                    });

                }
            )
            .catch(err => alert("Adding Check List ERROR.\nCheck Again your Network!"));
    }

    render(){

        return(
            <View style={{flex:1}}>
                <View style={{paddingHorizontal:20, height:60, width:'100%', backgroundColor:'white',flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                    <Image
                        style = {{height:35, width:100}}
                        source={require('../../assets/icleanheader.jpg')}
                    />
                </View>
                <ScrollView>
                    <View style= {{ flex:1, justifyContent: "center", alignItems: "center"}}>
                        <View style={{paddingVertical:20, width:'90%'}}>
                            <Text style={{color:'#56a5d3', fontWeight:'bold', fontSize:20}}>Auditing Process</Text>
                        </View>
                        <View style={{padding:10, width:'100%'}}>
                            <View style={{paddingBottom:10}}>
                                <Text style={{fontWeight:'bold'}}>{this.props.procedure.title}</Text>
                            </View>
                            <View style={{borderRadius: 5,padding:10, borderWidth:0.2}}>
                                <Text style={{}}>{this.props.procedure.subtitle}</Text>
                            </View>
                            <View style={{paddingTop:10}}>
                                <Text style={{fontWeight:'bold'}}>Comment:</Text>
                            </View>
                            <DefaultInput
                                placeholder=""
                                value={this.state.comment}
                                onChangeText={val => this.setState({comment: val})}
                                //valid={this.state.controls.email.valid}
                                //touched={this.state.controls.email.touched}
                                autoCapitalize="none"
                                autoCorrect={false}
                                keyboardType="email-address"
                                multiline={true}
                                numberOfLines={5}
                            />
                            <View style={{paddingTop:10}}>
                                <Text style={{fontWeight:'bold'}}>Check Status:</Text>
                            </View>
                            <Picker
                                selectedValue={this.state.checkstatus}
                                style={{ color: '#c5c5c5' }}
                                onValueChange={(itemValue, itemIndex) => {
                                    this.setState({ checkstatus: itemValue})
                                }
                                }>
                                <Picker.Item label="Not Checked" value="Not Checked"/>
                                <Picker.Item label="OK" value="OK"/>
                                <Picker.Item label="Need Improvement" value="Need Improvement"/>
                                <Picker.Item label="Failed" value="Failed"/>
                            </Picker>
                            <View style={{flexDirection:'row', alignItems:'center', paddingTop:10}}>
                                <Text style={{fontWeight:'bold', paddingRight:10, }}>Score:</Text>
                                <DefaultInput
                                    placeholder=""
                                    style={{paddingLeft: 20, width:60}}
                                    value={this.state.score}
                                    onChangeText={val => this.setState({score: val})}
                                    //valid={this.state.controls.email.valid}
                                    //touched={this.state.controls.email.touched}
                                    autoCapitalize="none"
                                    autoCorrect={false}
                                    keyboardType="numeric"
                                    multiline={false}
                                />
                            </View>
                            <View  style={{paddingTop:10,paddingBottom:5, paddingHorizontal:10, flexDirection: 'row', alignItems:'center', width: "100%",}}>
                                <Text style={{ color:"#56a5d3"}}></Text>
                            </View>
                            <PickImage
                                title="Image 1"
                                onImagePicked={this.imagePickedHandler1}
                            />
                            <PickImage
                                title="Image 2"
                                onImagePicked={this.imagePickedHandler2}
                            />
                            <PickImage
                                title="Image 3"
                                onImagePicked={this.imagePickedHandler3}
                            />

                            <View  style={{paddingTop:40, paddingBottom: 30, alignItems:'center'}}>
                                <TouchableOpacity onPress={this.submitAuditHandler}>
                                    <View style={{borderRadius: 5, paddingVertical:10,paddingHorizontal:40, backgroundColor:'#56a5d3'}}>
                                        <Text style={{paddingHorizontal:10, fontWeight:'bold', fontSize:16,color:'white'}}>SUBMIT AUDIT</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({

})

export default connect(null, null)(EditAudit);
